//
//  DetailView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    let detailItem: DetailModel
    
    var body: some View {
        GeometryReader { metrics in
            ZStack {
                RemoteImageView(withURL: self.detailItem.image_url)
                    .clipped()
                    .frame(width: metrics.size.width, height: metrics.size.height)
                
                VStack {
                    VStack {
                        HStack {
                            Text(self.detailItem.name)
                                .font(.custom("Palatino-Bold", size: 25))
                                .foregroundColor(.black)
                                .padding(EdgeInsets(top: 10.0, leading: 10.0, bottom: 10.0, trailing: 10.0))
                            
                            Spacer()
                        }
                        
                        if self.detailItem.about != "" {
                            HStack {
                                Text(self.detailItem.about)
                                    .font(.custom("Palatino-Bold", size: 18))
                                    .multilineTextAlignment(.leading)
                                    .foregroundColor(.secondary)
                                    .padding(EdgeInsets(top: 10.0, leading: 10.0, bottom: 10.0, trailing: 10.0))
                                
                                Spacer()
                            }
                        }
                        
                    }
                    .background(Color.white.opacity(0.3))
                    .cornerRadius(15.0)
                    .shadow(color: Color.gray.opacity(0.5), radius: 3, x: 5, y: 5)
                    .padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20))
                    
                    Spacer()
                }
            }
            .frame(width: metrics.size.width, height: metrics.size.height, alignment: .topLeading)
        }
        .edgesIgnoringSafeArea([.bottom])
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(detailItem: DetailModel.example)
    }
}
