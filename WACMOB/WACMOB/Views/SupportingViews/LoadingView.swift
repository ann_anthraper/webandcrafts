//
//  LoadingView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct LoadingView: View {

    @Binding var isShowing: Bool

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                VStack {
                    Text("Please wait ...")
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }
                .frame(width: geometry.size.width / 2,
                       height: geometry.size.height / 5)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(self.isShowing ? 1 : 0)

            }
        }
    }

}
