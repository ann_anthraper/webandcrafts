//
//  RemoteImageView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Combine
import SwiftUI

struct RemoteImageView: View {
    @ObservedObject var imageUtils: ImageUtilities
    
    init(withURL url:String) {
        imageUtils = ImageUtilities(withURL: url)
    }
    
    var body: some View {
        Image(uiImage: imageUtils.image ??  UIImage(named: "default_image")!)
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .clipped()
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        RemoteImageView(withURL: "")
    }
}
