//
//  HomeView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @State private var showDetailView = false
    
    @State var isDataRecieved = false
    
    @State var detail: DetailModel
    
    var body: some View {
        GeometryReader { metrics in
            if self.isDataRecieved {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        Image("online_shopping")
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                             .frame(width: metrics.size.width, height: metrics.size.height * 0.30, alignment: .center)
                            .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 20.0, trailing: 0.0))
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack(spacing: 0) {
                                ForEach(WACConstants.wac_offers, id: \.self) { item in
                                    OfferItemView(offer: item)
                                        .frame(width: metrics.size.width * 0.25, height: metrics.size.height * 0.30)
                                        .onTapGesture {
                                            self.detail = DetailModel(id: item.id, name: item.category, image_url: item.image_url, about: "")
                                            self.showDetailView = true
                                    }
                                }
                                .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 0.0, trailing: 0.0))
                            }
                             .frame(minWidth: 0, maxWidth: .infinity, minHeight:0, alignment: Alignment.leading)
                            
                        }
                        
                        Divider()
                            .padding(EdgeInsets(top: 20.0, leading: 10.0, bottom: 10.0, trailing: 10.0))
                        
                        HStack {
                            Text("Promotions")
                                .font(.custom("Palatino-Bold", size: 25))
                                .foregroundColor(.black)
                                .padding(EdgeInsets(top: 10.0, leading: 10.0, bottom: 10.0, trailing: 10.0))
                            
                            Spacer()
                            
                        }
                        .frame(width: metrics.size.width, height: metrics.size.height * 0.005, alignment: .center)
                        
                        
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack(spacing: 0) {
                                ForEach(WACConstants.wac_promotions, id: \.self) { item in
                                    PromotionItemView(promotion: item)
                                        .frame(width: metrics.size.width * 0.60, height: metrics.size.height * 0.40)
                                        .clipped()
                                        .onTapGesture {
                                            self.detail = DetailModel(id: item.id, name: item.name, image_url: item.image_url, about: item.about_promo)
                                            self.showDetailView = true
                                    }
                                }
                                .padding(EdgeInsets(top: 10.0, leading: 10.0, bottom: 0.0, trailing: 10.0))
                            }
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight:0, alignment: Alignment.leading)
                        }
                        
                        NavigationLink(destination: DetailView(detailItem: self.detail), isActive: self.$showDetailView) {
                            EmptyView()
                        }
                        
                        Spacer()
                        
                    }
                    
                    
                    
                    Spacer()
                    
                    Text("")
                    
                    Spacer()
                }
                .frame(width: metrics.size.width, height: metrics.size.height)
            } else {
                LoadingView(isShowing: .constant(!self.isDataRecieved))
            }
        }
        .onAppear(perform: {
            self.getMockupData()
        })
        
        .navigationBarTitle(Text("WACMOB").font(.subheadline),  displayMode: .inline)
    }
    
    //MARK:- Custom Methods
    
    func getMockupData() {
        WACDataManager.getMockupData(completionBlock: { (status, message) in
            if status {
                self.isDataRecieved = true
                print("\(message)")
            }
        }) { (error) in
            print("Mockup API failed with error : \(error)")
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(detail: DetailModel.example)
    }
}
