//
//  OfferItemView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct OfferItemView: View {
    
    let offer: OfferModel
    
    var body: some View {
        GeometryReader { metrics in
            VStack {
                RemoteImageView(withURL: self.offer.image_url)
                    .clipped()
                    .frame(width: metrics.size.width * 0.80 , height: metrics.size.height * 0.80)
                    .clipShape(Circle())
                    .shadow(color: Color.secondary, radius: 5, x: 0, y: 5)
                    .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 0.0, trailing: 0.0))
                
                Text(self.offer.category)
                    .font(.custom("Palatino", size: 20))
                    .lineLimit(2)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.black)
                    .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 10.0, trailing: 0.0))
            }
        }
    }
}

