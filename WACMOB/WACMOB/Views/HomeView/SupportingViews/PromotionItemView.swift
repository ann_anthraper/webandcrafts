//
//  PromotionItemView.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct PromotionItemView: View {
    
    var promotion: PromotionModel
    
    var body: some View {
        GeometryReader { metrics in
            VStack {
                RemoteImageView(withURL: self.promotion.image_url)
                    .clipped()
                    .frame(width: metrics.size.width, height: metrics.size.height * 0.50)
                    .padding(EdgeInsets(top: 0.0, leading: 0.0, bottom: 0.0, trailing: 0.0))
                    .cornerRadius(10)
                
                
                HStack {
                    Text(self.promotion.name)
                        .font(.custom("Palatino", size: 20))
                        .fontWeight(.bold)
                        .lineLimit(1)
                        .foregroundColor(.black)
                        .padding(EdgeInsets(top: 0.0, leading: 10.0, bottom: 10.0, trailing: 0.0))
                    
                    Spacer()
                }
                
                HStack {
                    Text(self.promotion.about_promo)
                        .font(.custom("Palatino", size: 15))
                        .lineLimit(1)
                        .foregroundColor(.secondary)
                        .padding(EdgeInsets(top: 0.0, leading: 10.0, bottom: 20.0, trailing: 0.0))
                    
                    Spacer()
                }
            }
            .frame(width: metrics.size.width, height: metrics.size.height)
        }
    }
}
