//
//  WACInteractor.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation

class WACInteractor {
    
    static func getMockupData(successfulResponse:@escaping((Any) -> ()), failure:@escaping((NSError) -> ())) {
        WACRestManager.getMockupData { (response) in
            switch response.result {
            case .success(_) :
                do {
                    if let responseData = response.result.value {
                        do {
                            let result = try JSONSerialization.jsonObject(with: responseData, options: .mutableLeaves)
                            successfulResponse(result)
                        } catch {
                            // handle error
                        }
                    }
                }
            case .failure(let error):
                let networkError = NSError(domain: "Error", code: error._code, userInfo: nil)
                failure(networkError)
            }
        }
    }
    
}
