//
//  OfferModel.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct OfferModel: Hashable, Identifiable {
    
    var id: Int
    var category: String
    var image_url: String
    
}

