//
//  PromotionModel.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct PromotionModel: Hashable, Identifiable {
    
    var id: Int
    var name: String
    var image_url: String
    var about_promo: String
    
}

