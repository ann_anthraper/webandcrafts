# WebandCrafts

Prototype of a Quiz app for iOS.

**Requirements**
* Xcode 11.0
* iOS 13.0

**How to run the example?**
1. Clone the project from https://gitlab.com/ann_anthraper/webandcrafts.git repo 
2. Open shell window and navigate to project folder
3. Run pod install
4. Open WACMOB.xcworkspace and run the project on simulator.

**Screenshots**
![Image of Splash Screen](Screenshots/splash screen.png)
![Image of Home View](Screenshots/home view.png)
![Image of Detail View](Screenshots/detail view.png)

**Third Party Libraries**
1. Alamofire : used for REST API web services.
2. SwiftyJSON : used to parse JSON data.


